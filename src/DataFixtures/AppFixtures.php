<?php

namespace App\DataFixtures;

use App\Entity\Post;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder) {
        $this->encoder = $encoder;
    }
    
    public function load(ObjectManager $manager)
    {
        for ($i=1; $i < 5; $i++) { 

            $user = new User();
            $hashedPassword = $this->encoder->encodePassword($user, '1234');

            $user->setEmail('mail'.$i.'@mail.com')
            ->setPassword($hashedPassword)
            ->setRoles(['ROLE_USER']);

            for ($y=1; $y < 5; $y++) { 
                $post = new Post();
                $post->setTitle("Title ".$y)
                ->setContent("The content of the post number ".$y . "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum")
                ->setAuthor($user)
                ;
                $manager->persist($post);
            }
            $manager->persist($user);
        }

        $user = new User();
        $hashedPassword = $this->encoder->encodePassword($user, 'admin');

        $user->setEmail('admin@admin.com')
        ->setPassword($hashedPassword)
        ->setRoles(['ROLE_ADMIN']);
        $manager->persist($user);


        $manager->flush();
    }
}
